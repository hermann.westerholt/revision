erf = @(x) (1/2 + 1/sqrt(2*pi)*exp(-power(x,2)/2).*(x + power(x,3)/3 + power(x,5)/15 + power(x,7)/105 + power(x,9)/945 + power(x,11)/10395 + power(x,13)/135135 + power(x,15)/2027025 + power(x,17)/34459425 + power(x,19)/654729075 + power(x,21)/13749310575 + power(x,23)/316234143225));% + power(x,25)/7905853580625 + power(x,27)/213458046676875));
x = linspace(-2,2,10001);
y = erf(x);
diff = erf(x)-normcdf(x);
maxErr = max(diff)
minErr = min(diff)
idxMax = find(diff == maxErr)
idxMin = find(diff == minErr)