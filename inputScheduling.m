function [set_w,set_h,set_rec,set_lim,set_constr,set_i,param_Pth0,...
    param_PthVar,param_PRL,param_PRLProdLoss,param_eta0,param_meanae,param_maxae,...
    param_maxre,param_EthMax,param_m,param_b,param_PLinMin,param_PLinMax,param_DAP,...
    param_AlP,param_PRLSP] = ...
    inputScheduling(perturbMode,firstWeek,lastWeek,Pth0,PthVar,eta0,etaVar,etaNodes,EthMax,PRL,PRLSP,delPRL,delProdLoss,ProdLossMode)
%WRITEGMS Creates input data ... blaaaa

if etaVar==0
    m=eta0;
    b=0;
    PthLinMin = Pth0-PthVar;
    PthLinMax = Pth0+PthVar;
    meanae = 0;
    maxae = 0;
    maxre = 0;
    etaNodes = 1;
else
    [m,b,PthLinMin,PthLinMax,meanae,maxae,maxre] = pwlf(Pth0,PthVar,eta0,etaVar,etaNodes);
end
PRLProdLoss = prlProdLoss(PRL,eta0,Pth0,delPRL,delProdLoss,ProdLossMode);
[DAP,PRLSPMed,PRLSPMar,AlP] = xlsSched(perturbMode,firstWeek,lastWeek);
if strcmp(PRLSP,'Median')
    PRLSPval = PRLSPMed;
elseif strcmp(PRLSP,'Margin')
    PRLSPval = PRLSPMar;
else
    PRLSPval = 0;
end
%% Helper function
guel = @(s,v) strcat(s,strsplit(num2str(v)));

%% Input data
set_w = createGDXset('w',{guel('',firstWeek:lastWeek)},'initialize');
set_h = createGDXset('h',{guel('',1:168)},'initialize');
set_rec = createGDXset('rec',{{'yearly','weekly'}},'initialize',[1:2]');
set_lim = createGDXset('lim',{{'unlimited','limited'}},'initialize',[1:2]');
set_constr = createGDXset('constr',[set_rec.uels,set_w.uels],'initialize',...
    vertcat([1,numel(firstWeek:lastWeek)],[2*ones(numel(firstWeek:lastWeek),1),(1:numel(firstWeek:lastWeek))']));
set_i = createGDXset('i',{guel('',1:etaNodes)},'initialize');

param_Pth0 = createGDXparameter('Pth0',Pth0,'full',0,'initialize');
param_PthVar = createGDXparameter('PthVar',PthVar,'full',0,'initialize');
param_PRL = createGDXparameter('PRL',PRL.*ones(numel(firstWeek:lastWeek),1),'full',1,'initialize',set_w.uels);
param_PRLProdLoss = createGDXparameter('PRLProdLoss',PRLProdLoss.*ones(numel(firstWeek:lastWeek),1),'full',1,'initialize',set_w.uels);
param_eta0 = createGDXparameter('eta0',eta0,'full',0,'initialize');
param_meanae = createGDXparameter('meanae',meanae,'full',0,'initialize');
param_maxae = createGDXparameter('maxae',maxae,'full',0,'initialize');
param_maxre = createGDXparameter('maxre',maxre,'full',0,'initialize');
param_EthMax = createGDXparameter('EthMax',[inf,EthMax]','full',...
    1,'initialize',set_lim.uels);
param_m = createGDXparameter('m',m','full',1,'initialize',set_i.uels);
param_b = createGDXparameter('b',b','full',1,'initialize',set_i.uels);
param_PLinMin = createGDXparameter('PthLinMin',PthLinMin','full',1,'initialize',set_i.uels);
param_PLinMax = createGDXparameter('PthLinMax',PthLinMax','full',1,'initialize',set_i.uels);
param_DAP = createGDXparameter('DAP',DAP,'full',2,'initialize',[set_w.uels,set_h.uels]);
param_AlP = createGDXparameter('AlP',AlP,'full',1,'initialize',set_w.uels);
param_PRLSP = createGDXparameter('PRLSP',PRLSPval,'full',1,'initialize',set_w.uels);
end