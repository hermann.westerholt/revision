function [DAP,PRLSPMed,PRLSPMar,AlP] = xlsSched(perturbMode,firstWeek,lastWeek)
    file = ['Perturb.xlsx'];
    dapRange = 'A2:G25';
    otherRange = 'H2:J2';
    for i = firstWeek:lastWeek
        sheet = ['Week',int2str(i),perturbMode];
        dapX = xlsread(file,sheet,dapRange);
        otherX = xlsread(file,sheet,otherRange);
        dapX = reshape(dapX,1,168);
        if i == firstWeek
            DAP = dapX;
            other = otherX;
        else
            DAP = vertcat(DAP,dapX);
            other = vertcat(other,otherX);
        end
    end
    PRLSPMed = other(:,1);
    PRLSPMar = other(:,2);
    AlP = other(:,3);
end