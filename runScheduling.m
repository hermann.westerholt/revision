clear all
atHome = false;
myPath = pwd;
winPath = 'C:\Users\hermannwesterholt\Documents\MATLAB';
tStart0 = tic;
if ~atHome
    addpath 'C:\GAMS\win64\25.0.1\';
end

%% Choose parameters
% Load constants
[Pth0, delPRL, EthMax] = loadConstants();
% Choose delProdLoss from [0 0.5/100 1/100], default is 0.5/100
delProdLoss = 0.5/100;
% Choose ProdLossMode from ['q','l'], default is 'q'
ProdLossMode = 'q';
% Choose etaVar from [0 1/100 2/100], default is 1/100
etaVar = 1/100;
% Choose scenario 'BaseCase' or 'AluPurch'
scenarioList = {'BaseCase'};
% Choose year [2016,2018]
year = 2018;
% Choose first week and last week
firstWeek = 1;
lastWeek = 6;
% PRL quantity [0,...,PVar]
PRLList = [0,10,20];
% PRL service price
PRLSP = 'Median';
% conversion rate eta at Pth0 [t/MWh]
eta0 = 1/14;
% number of nodes for piece-wise linear function
etaNodesList = [15];
% fprintflay output settings
count = 0;
numLoops = length(scenarioList)*length(PRLList)*length(etaNodesList);
timeFormat = '%02d:%05.2f';
txtFormat = '%02d';
eta0Format = '%04.1f';
etaVarFormat = '%05.3f';

%% Loop
for scenarioStr = scenarioList
    scenario = scenarioStr{1};
    for PRL = PRLList
        PthVar = 0.25*Pth0-PRL;
        for etaNodes = etaNodesList
            %% Create input file and copy to input folder
            tStart1 = tic;
            count = count+1;
            fprintf(['\nBeginning run (' sprintf(txtFormat,count) '/' sprintf(txtFormat,numLoops) '):     Scenario=' scenario ', PRL=' sprintf(txtFormat,PRL) ', etaNodes=' sprintf(txtFormat,etaNodes) '\n']);
            if PRL>22.5
                error('PRL cannot exceed 22.5 MW!');
            end
            [set_w,set_h,set_rec,set_lim,set_constr,set_i,param_Pth0,...
                param_PthVar,param_PRL,param_PRLProdFactor,param_eta0,param_meanae,...
                param_maxae,param_maxre,param_EthMax,param_m,param_b,param_PLinMin,param_PLinMax,param_DAP,...
                param_AlP,param_PRLSP] = ...
                inputScheduling(year,firstWeek,lastWeek,Pth0,PthVar,eta0,etaVar,etaNodes,EthMax,PRL,PRLSP,delPRL,delProdLoss,ProdLossMode);

            %% Create model file
            if strcmp(scenario,'BaseCase')
                writeGmsBaseCase();
                gdxFileName = [scenario '_PRL=' sprintf(txtFormat,PRL) '_eta0^-1=' sprintf(eta0Format,1/eta0) '_etaVar=' sprintf(etaVarFormat,etaVar) '_n=' sprintf(txtFormat,etaNodes) '_delProdLoss=' sprintf(etaVarFormat,delProdLoss) '_' ProdLossMode];
            elseif strcmp(scenario,'AluPurch')
                writeGmsAluPurch();
                gdxFileName = [scenario '_PRL=' sprintf(txtFormat,PRL) '_' PRLSP '_eta0^-1=' sprintf(eta0Format,1/eta0) '_etaVar=' sprintf(etaVarFormat,etaVar) '_n=' sprintf(txtFormat,etaNodes) '_delProdLoss=' sprintf(etaVarFormat,delProdLoss) '_' ProdLossMode];
            else
                error("Chosen scenario does not exist!");
            end

            %% Run simulation
            if atHome
                copyfile([scenario '.gms'],winPath);
                copyfile('prices.gdx',winPath);
                cd(winPath);
            end
            
            if strcmp(scenario,'BaseCase')
                gams(scenario,set_w,set_h,set_rec,set_lim,set_constr,set_i,param_Pth0,...
                    param_PthVar,param_PRL,param_PRLProdFactor,param_eta0,param_meanae,...
                    param_maxae,param_maxre,param_EthMax,param_m,param_b,param_PLinMin,param_PLinMax,param_DAP,...
                    param_AlP);
            elseif strcmp(scenario,'AluPurch')
                gams(scenario,set_w,set_h,set_rec,set_lim,set_constr,set_i,param_Pth0,...
                    param_PthVar,param_PRL,param_PRLProdFactor,param_eta0,param_meanae,...
                    param_maxae,param_EthMax,param_m,param_b,param_PLinMin,param_PLinMax,param_DAP,...
                    param_AlP,param_PRLSP);
            end

            %% Clean up
            if atHome
                delete('prices.gdx');
                delete([scenario '.gms']);
                if exist([scenario '.log'],'file')==2
                    movefile ([scenario '.log'],myPath);
                end
                if exist([scenario '.lst'],'file')==2
                    movefile ([scenario '.lst'],myPath);
                end
                if exist([scenario '.lxi'],'file')==2
                    movefile ([scenario '.lxi'],myPath);
                end
                movefile ('matsol.gdx',myPath);
                movefile ('matdata.gms',myPath);
                movefile ('matdata.gdx',myPath);
                cd(myPath);
            end
            movefile ([scenario '.gms'],['1_GMS' filesep scenario '.gms']);
            if exist([scenario '.log'],'file')==2
                movefile ([scenario '.log'],[ '1_GMS' filesep scenario '.log']);
            end
            if exist([scenario '.lst'],'file')==2
                movefile ([scenario '.lst'],[ '1_GMS' filesep scenario '.lst']);
            end
            if exist([scenario '.lxi'],'file')==2
                movefile ([scenario '.lxi'],[ '1_GMS' filesep scenario '.lxi']);
            end
            movefile ('matsol.gdx',[ '2_GDX_Output' filesep gdxFileName '.gdx']);
            movefile ('matdata.gms',[ '1_GMS' filesep 'matdata.gms']);
            movefile ('matdata.gdx',[ '1_GMS' filesep 'matdata.gdx']);
            t0=toc(tStart0);
            t1=toc(tStart1);
            fprintf(['Run (' sprintf(txtFormat,count) '/' sprintf(txtFormat,numLoops) ') solved in:     ' sprintf(timeFormat,floor(t1/60),rem(t1,60)) ' min.\n']);
            fprintf(['Elapsed time since launch: ' sprintf(timeFormat,floor(t0/60),rem(t0,60)) ' min.\n']);
        end
    end
end

%% Done
fprintf('\nDONE!\n');