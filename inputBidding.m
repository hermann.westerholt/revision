function [set_scenIdx, set_ii, set_map, param_mu, param_sigm, param_C0, ...
    param_PRL, param_CPRL,param_movAvg,param_f2] = inputBidding(C0, CPRL,...
    PRLvec, PRLcell, perturbMode, sigmFactor, bidWeek, idxBidding, bidMode)

    guel = @(s, v) strcat(s, strsplit(num2str(v)));
    
    [muPRL,sigPRL,muLogF1,sigmLogF1,movAvg,f2] = xlsBid(perturbMode,bidWeek);
    
    if strcmp(bidMode,'estimPrices')
        mu = muLogF1;
        sigm = sigmLogF1*sigmFactor;
    elseif strcmp(bidMode,'knownPrices')
        mu = muPRL;
        sigm = sigPRL*sigmFactor;
    end
    
    param_mu = createGDXparameter('mu', mu, 'full', 0, 'initialize');
    param_sigm = createGDXparameter('sigm', sigm, 'full', 0, 'initialize');
    param_C0 = createGDXparameter('C0', C0(idxBidding), 'full', 0, 'initialize');

    [scenIdx, setMax, mapVal, PRLval, CPRLval] = biddingScenarios(PRLvec, PRLcell, CPRL, idxBidding);

    set_scenIdx = createGDXset('scenIdx', {guel('', 1:scenIdx)}, 'initialize');
    set_ii = createGDXset('ii', {guel('', 1:setMax)}, 'initialize');
    set_map = createGDXset('map', [set_scenIdx.uels, set_ii.uels], ...
        'initialize', mapVal);
    param_PRL = createGDXparameter('allPRL', PRLval, 'sparse', ...
        2, 'initialize', [set_scenIdx.uels, set_ii.uels]);
    param_CPRL = createGDXparameter('allCPRL', CPRLval, 'sparse', 2, 'initialize', ...
        [set_scenIdx.uels, set_ii.uels]);
    param_movAvg = createGDXparameter('movAvg',movAvg,'full',0,'initialize');
    param_f2 = createGDXparameter('f2',f2,'full',0,'initialize');
end