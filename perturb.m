clear all
atHome = false;
myPath = pwd;
winPath = 'C:\Users\hermannwesterholt\Documents\MATLAB';
tStart0 = tic;
if ~atHome
    addpath 'C:\GAMS\win64\25.0.1\';
end

%% Choose parameters
% Load constants
[Pth0, delPRL, EthMax] = loadConstants();
% Choose delProdLoss from [0 0.5/100 1/100], default is 0.5/100
delProdLossList = [0.5/100];
% Choose ProdLossMode from ['q','l'], default is 'q'
ProdLossMode = 'q';
% Choose etaVar from [0 1/100 2/100], default is 1/100
etaVarList = [1/100];
% Choose perturbation ['normal','high','low']
perturbMode = 'normal';
% Choose first week and last week for scheduling
firstWeek = 1;
lastWeek = 4;
% Choose first week and last week for scheduling
firstWeekBidding = 4;
lastWeekBidding  = 4;
% Total PRL offered
PRLtot = [1:10];
% Smallest PRL offer quantity
PRLmin = 1;
% Max number of PRL offer split elements
PRLsplit = 3;
% conversion rate eta at Pth0 [t/MWh]
eta0 = 1/14;
% number of nodes for piece-wise linear function
etaNodes = 15;
% Bidding mode ['knownPrices','estimPrices']
bidMode = 'estimPrices';
% Choose sigmFactor from [0.5, 1, 2], default is 1
sigmFactor = 1;
% fprintflay output settings
count = 0;
timeFormat = '%02d:%05.2f';
txtFormat = '%02d';
eta0Format = '%04.1f';
etaVarFormat = '%05.3f';

%% initialize solution vectors
guel = @(s,v) strcat(s,strsplit(num2str(v)));
c.name = 'C';
c.form = 'full';
c.uels = {guel('',firstWeek:lastWeek)};
p.name = 'askPrice';

for etaVar=etaVarList
for delProdLoss=delProdLossList
    count = 0;
%% Create model file
writeGmsAluPurchNoPrl();

%% Calculate production cost without PRL
tStart1 = tic;
PRL = 0;
fprintf(['\nBeginning run:             PRL=' sprintf(txtFormat,PRL) ', etaNodes=' sprintf(txtFormat,etaNodes) '\n']);
PthVar = 0.25*Pth0 - PRL;
gdxFileName = ['SchedBid_' perturbMode '_PRL=' sprintf(txtFormat,PRL) '_eta0^-1=' sprintf(eta0Format,1/eta0) '_etaVar=' sprintf(etaVarFormat,etaVar) '_n=' sprintf(txtFormat,etaNodes) '_delProdLoss=' sprintf(etaVarFormat,delProdLoss) '_' ProdLossMode];

% Create input data for scheduling
[set_w,set_h,set_rec,set_lim,set_constr,set_i,param_Pth0,...
    param_PthVar,param_PRL,param_PRLProdLoss,param_eta0,param_meanae,...
    param_maxae,param_maxre,param_EthMax,param_m,param_b,param_PLinMin,param_PLinMax,param_DAP,...
    param_AlP] = ...
    inputScheduling(perturbMode,firstWeek,lastWeek,Pth0,PthVar,eta0,etaVar,etaNodes,EthMax,PRL,'',delPRL,delProdLoss,ProdLossMode);

% Run gams scheduling
if atHome
    copyfile('AluPurchNoPRL.gms',winPath);
    copyfile('prices.gdx',winPath);
    cd(winPath);
 end
gams('AluPurchNoPRL',set_w,set_h,set_rec,set_lim,set_constr,set_i,param_Pth0,...
    param_PthVar,param_PRL,param_PRLProdLoss,param_eta0,param_meanae,...
    param_maxae,param_maxre,param_EthMax,param_m,param_b,param_PLinMin,param_PLinMax,...
    param_DAP,param_AlP);

% Get value C
c = rgdx('matsol.gdx',c);
C0 = c.val;
c = rmfield(c,'type');
c = rmfield(c,'dim');
c = rmfield(c,'val');
if atHome
    cd(myPath);
end
movefile ('matsol.gdx',['2_GDX_Output' filesep gdxFileName '.gdx']);
movefile ('matdata.gms',['1_GMS' filesep 'SchedBidMatdata.gms']);
movefile ('matdata.gdx',['1_GMS' filesep 'SchedBidMatdata.gdx']);
t1 = toc(tStart1);
t0 = toc(tStart0);
fprintf(['Run solved in:             ' sprintf(timeFormat,floor(t1/60),rem(t1,60)) ' min.\n']);
fprintf(['Elapsed time since launch: ' sprintf(timeFormat,floor(t0/60),rem(t0,60)) ' min.\n']);

%% PRL partitions
PRLcheck = find(PRLmin > PRLtot);
if ~isempty(PRLcheck)
    error('PRLmin is larger than at least one element in PRLtot!');
end

[PRLvec,PRLcell] = prlPartitions(PRLtot,PRLmin,PRLsplit);
numLoops = length(PRLvec);
CPRL = zeros(numel(firstWeek:lastWeek),numLoops);

%% Loop
for PRL = PRLvec
    PthVar = 0.25*Pth0-PRL;
    if PRL>22.5
        error('PRL cannot exceed 22.5 MW!');
    end
    tStart2 = tic;
    count = count+1;
    gdxFileName = ['SchedBid' '_PRL=' sprintf(txtFormat,PRL) '_eta0^-1=' sprintf(eta0Format,1/eta0) '_etaVar=' sprintf(etaVarFormat,etaVar) '_n=' sprintf(txtFormat,etaNodes) '_delProdLoss=' sprintf(etaVarFormat,delProdLoss) '_' ProdLossMode];
    fprintf(['\nBeginning run (' sprintf(txtFormat,count) '/' sprintf(txtFormat,numLoops) '):     PRL=' sprintf(txtFormat,PRL) ', etaNodes=' sprintf(txtFormat,etaNodes) '\n']);

    %% PRL=currentValue
    % Create input data for scheduling
    [set_w,set_h,set_rec,set_lim,set_constr,set_i,param_Pth0,...
        param_PthVar,param_PRL,param_PRLProdLoss,param_eta0,param_meanae,...
        param_maxae,param_maxre,param_EthMax,param_m,param_b,param_PLinMin,param_PLinMax,param_DAP,...
        param_AlP] = ...
        inputScheduling(perturbMode,firstWeek,lastWeek,Pth0,PthVar,eta0,etaVar,etaNodes,EthMax,PRL,'',delPRL,delProdLoss,ProdLossMode);

    % Run gams scheduling
    if atHome
        copyfile('AluPurchNoPRL.gms',winPath);
        copyfile('prices.gdx',winPath);
        cd(winPath);
     end
    gams('AluPurchNoPRL',set_w,set_h,set_rec,set_lim,set_constr,set_i,param_Pth0,...
        param_PthVar,param_PRL,param_PRLProdLoss,param_eta0,param_meanae,...
        param_maxae,param_maxre,param_EthMax,param_m,param_b,param_PLinMin,param_PLinMax,...
        param_DAP,param_AlP);

    % Get value C
    c = rgdx('matsol.gdx',c);
    CPRL(:,count) = c.val;
    c = rmfield(c,'type');
    c = rmfield(c,'dim');
    c = rmfield(c,'val');
    movefile ('matsol.gdx',['2_GDX_Output' filesep gdxFileName '.gdx']);
    movefile ('matdata.gms',['1_GMS' filesep 'SchedBidMatdata.gms']);
    movefile ('matdata.gdx',['1_GMS' filesep 'SchedBidMatdata.gdx']);
    if atHome
        cd(myPath);
    end
    t0=toc(tStart0);
    t2=toc(tStart2);
    fprintf(['Run (' sprintf(txtFormat,count) '/' sprintf(txtFormat,numLoops) ') solved in:     ' sprintf(timeFormat,floor(t2/60),rem(t2,60)) ' min.\n']);
    fprintf(['Elapsed time since launch: ' sprintf(timeFormat,floor(t0/60),rem(t0,60)) ' min.\n']);
end
fprintf('\nScheduling succesfully terminated.\n');

%% Bidding
for j = firstWeekBidding:lastWeekBidding
    tStart3 = tic;
    bidWeek = j;
    
    %% Create model file
    writeGmsBid(bidMode);
    
    %% File name definition
    gdxFileName = ['BidSched_' perturbMode '_w=' sprintf(txtFormat,bidWeek) '_sigm=' sprintf(txtFormat,sigmFactor) '_eta0^-1=' sprintf(eta0Format,1/eta0) '_etaVar=' sprintf(etaVarFormat,etaVar) '_n=' sprintf(txtFormat,etaNodes) '_delProdLoss=' sprintf(etaVarFormat,delProdLoss) '_' ProdLossMode '_' bidMode];
    idxBidding = find(firstWeek:lastWeek == bidWeek);
    % Create input data
    [set_scenIdx,set_ii,set_map,param_mu,param_sigm,param_C0,param_PRL,...
        param_CPRL,param_movAvg,param_f2] = inputBidding(C0,CPRL,PRLvec,PRLcell,...
        perturbMode, sigmFactor,bidWeek,idxBidding,bidMode);

    %% Run gams bidding
    fprintf(['\nCalculating bidding strategy ...\n']);
    if atHome
        copyfile('Bidding.gms',winPath);
        cd(winPath);
    end
    %gamso.show = 'normal';
    if strcmp(bidMode,'knownPrices')
        gams('Bidding',set_scenIdx,set_ii,set_map,param_mu,param_sigm,param_C0,...
            param_PRL,param_CPRL);
    elseif strcmp(bidMode,'estimPrices')
        gams('Bidding',set_scenIdx,set_ii,set_map,param_mu,param_sigm,param_C0,...
            param_PRL,param_CPRL,param_movAvg,param_f2);
    end
    t3=toc(tStart3);
    fprintf(['Bidding solved in ' sprintf(timeFormat,floor(t3/60),rem(t3,60)) ' min.\n']);
    
    %% Clean up
    if atHome
        if exist('AluPurchNoPRL.log','file')==2
            movefile ('AluPurchNoPRL.log',myPath);
        end
        if exist('AluPurchNoPRL.lst','file')==2
            movefile ('AluPurchNoPRL.lst',myPath);
        end
        if exist('AluPurchNoPRL.lxi','file')==2
            movefile ('AluPurchNoPRL.lxi',myPath);
        end
        if exist('Bidding.log','file')==2
            movefile ('Bidding.log',myPath);
        end
        if exist('Bidding.lst','file')==2
            movefile ('Bidding.lst',myPath);
        end
        if exist('Bidding.lxi','file')==2
            movefile ('Bidding.lxi',myPath);
        end
        movefile ('matsol.gdx',myPath);
        movefile ('matdata.gms',myPath);
        movefile ('matdata.gdx',myPath);
        cd(myPath);
    end
    if exist('AluPurchNoPRL.gms','file')==2
    movefile ('AluPurchNoPRL.gms',['1_GMS' filesep 'AluPurchNoPRL.gms']);
    end
    if exist('AluPurchNoPRL.log','file')==2
    movefile ('AluPurchNoPRL.log',['1_GMS' filesep 'AluPurchNoPRL.log']);
    end
    if exist('AluPurchNoPRL.lst','file')==2
    movefile ('AluPurchNoPRL.lst',['1_GMS' filesep 'AluPurchNoPRL.lst']);
    end
    if exist('AluPurchNoPRL.lxi','file')==2
    movefile ('AluPurchNoPRL.lxi',['1_GMS' filesep 'AluPurchNoPRL.lxi']);
    end

    movefile ('Bidding.gms',['1_GMS' filesep 'Bidding.gms']);
    if exist('Bidding.log','file')==2
    movefile ('Bidding.log',['1_GMS' filesep 'Bidding.log']);
    end
    if exist('Bidding.lst','file')==2
    movefile ('Bidding.lst',['1_GMS' filesep 'Bidding.lst']);
    end
    if exist('Bidding.lxi','file')==2
    movefile ('Bidding.lxi',['1_GMS' filesep 'Bidding.lxi']);
    end
    movefile ('matsol.gdx',['2_GDX_Output' filesep gdxFileName '.gdx']);
    movefile ('matdata.gms',['1_GMS' filesep 'BidSchedMatdata.gms']);
    movefile ('matdata.gdx',['1_GMS' filesep 'BidSchedMatdata.gdx']);

end
end
end
t4=toc(tStart0);
fprintf(['Elapsed time since launch: ' sprintf(timeFormat,floor(t4/60),rem(t4,60)) ' min.\n']);

%% Done
fprintf(['\nDONE!\n']);