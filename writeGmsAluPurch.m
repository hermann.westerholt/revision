function [] = writeGmsAluPurch()
%WRITEGMS Creates a .gms file with input-dependent symbol declarations. The
%   assignment of input values to parameters is done in a separate step.

%% Write .gms file
fileID = fopen('AluPurch.gms','w');
fprintf(fileID,'$set matout "''matsol.gdx''";\n');

%% Define Sets
fprintf(fileID,'Sets\n');
fprintf(fileID,'  w              Weeks in a year\n');
fprintf(fileID,'  h              Hours in a week\n');
fprintf(fileID,'  rec            Energy balance reconciliation intervall\n');
fprintf(fileID,'  lim            Toggle for limit on energy surplus\n');
fprintf(fileID,'  constr(rec,w)  All constraints based on reconciliation intervalls\n');
fprintf(fileID,'  currConstr(w)  Constraints used in current loop\n');
fprintf(fileID,'  i              Intervalls for linearization\n');
fprintf(fileID,'  stat           Set for model and solver status /modelstat,solvestat/;\n');
fprintf(fileID,';\n');

%% Define Scalars
fprintf(fileID,'\nScalars\n');
fprintf(fileID,'  Pth0    Nominal power consumption [MW]\n');
fprintf(fileID,'  PthVar  Maximal allowed power variation [MW]\n');
fprintf(fileID,'  eta0    Nominal production efficiency rate [t per MWh]\n');
fprintf(fileID,'  meanae  Mean absolute error of linearization\n');
fprintf(fileID,'  maxae   Max absolute error of linearization\n');
fprintf(fileID,'  maxre   Max relative error of linearization\n');
fprintf(fileID,';\n');

%% Define Parameters
fprintf(fileID,'\nParameters \n');
fprintf(fileID,'  EthMax(lim)       Maximal allowed thermal energy surplus [MWh]\n');
fprintf(fileID,'  DAP(w,h)          Day-Ahead electricity price for each hour of the year [EUR per MW]\n');
fprintf(fileID,'  AlP(w)            Average aluminium price for each week [EUR per t]\n');
fprintf(fileID,'  PRL(w)            Primary power reserve in week w [MW]\n');
fprintf(fileID,'  PRLSP(w)          PRL service price [EUR per MW]\n');
fprintf(fileID,'  PRLProdLoss(w)    Weekly production loss attributed to PRL [t]\n');
fprintf(fileID,'  PthLinMin(i)      Lower bound in intervall i\n');
fprintf(fileID,'  PthLinMax(i)      Upper bound in intervall i\n');
fprintf(fileID,'  m(i)              Slope in intervall i\n');
fprintf(fileID,'  b(i)              Intercept in intervall i\n');
fprintf(fileID,'  returnStat(stat)  Parameter for model and solver status;\n');
fprintf(fileID,';\n');

%% Load input data
fprintf(fileID,'\n$if exist matdata.gms $include matdata.gms\n');

%% Define Variables
fprintf(fileID,'\nVariables \n');
fprintf(fileID,'  Pth(w,h)             Power uptake in hour h [MW]\n');
fprintf(fileID,'  PthLin(w,h,i)        Power uptake in current intervall in hour h [MW]\n');
fprintf(fileID,'  dmal(w,h)            Marginal aluminium production in hour h [t per h]\n');
fprintf(fileID,'  Eth(w,h)             Accumulated thermal energy surplus [MWh]\n');
fprintf(fileID,'  mal(w,h)             Accumulated aluminium production [t]\n');
fprintf(fileID,'  C(w)                 Weekly production cost [EUR]\n');
fprintf(fileID,'  Ctot                 Total cost [EUR]\n');
fprintf(fileID,';\n');

fprintf(fileID,'\nBinary variables \n');
fprintf(fileID,'  y(w,h,i)             Indicator for active intervall\n');
fprintf(fileID,';\n');

fprintf(fileID,'\nPositive variables \n');
fprintf(fileID,'  mPurch(w)            Amount of purchased aluminium in week w [t]\n');
fprintf(fileID,';\n');

%% Define Equations
fprintf(fileID,'\nEquations \n');
fprintf(fileID,'  CostTotal              Total production cost\n');
fprintf(fileID,'  CostWeek(w)            Weekly production cost\n');
fprintf(fileID,'  Start_th(w)            Thermal energy surplus in first hour of every week has to equal zero\n');
fprintf(fileID,'  Start_al(w)            Production surplus in first hour of every week has to equal zero\n');
fprintf(fileID,'  Storage_th(w,h)        Thermal energy surplus in hour h\n');
fprintf(fileID,'  Storage_al(w,h)        Production surplus in hour h\n');
fprintf(fileID,'  Reconc(w)              Force Eth to return to zero in regular intervalls\n');
fprintf(fileID,'  LinearClosing(w,h)     Only one mode can be active\n');
fprintf(fileID,'  LinearSum(w,h)         Sum over piecewise linear parts\n');
fprintf(fileID,'  LinearConstrLo(w,h,i)  Minimum x value of current section of pwlf\n');
fprintf(fileID,'  LinearConstrUp(w,h,i)  Maximum x value of current section of pwlf\n');
fprintf(fileID,'  dmalu(w,h)             Marginal aluminium production in hour h\n');
fprintf(fileID,'  AluTarget(w)           Production target for each week\n');
fprintf(fileID,';\n');

fprintf(fileID,'  CostTotal                         ..  Ctot =e= sum(w,C(w));\n');
fprintf(fileID,'  CostWeek(w)                       ..  C(w) =e= sum(h, Pth(w,h) * DAP(w,h)) + mPurch(w) * AlP(w) - PRL(w) * PRLSP(w);\n');
fprintf(fileID,'  Start_th(w)                       ..  Eth(w,"1") =e= 0;\n');
fprintf(fileID,'  Start_al(w)                       ..  mal(w,"1") =e= 0;\n');
fprintf(fileID,'  Storage_th(w,h)$(ord(h)<card(h))  ..  Eth(w,h+1) =e= Eth(w,h) + Pth(w,h) - Pth0;\n');
fprintf(fileID,'  Storage_al(w,h)$(ord(h)<card(h))  ..  mal(w,h+1) =e= mal(w,h) + dmal(w,h);\n');
fprintf(fileID,'  Reconc(w)$currConstr(w)           ..  Pth(w,"168") - Pth0 =e= -Eth(w,"168");\n');
fprintf(fileID,'  LinearClosing(w,h)                ..  1 =e= sum(i,y(w,h,i));\n');
fprintf(fileID,'  LinearSum(w,h)                    ..  Pth(w,h) =e= sum(i,PthLin(w,h,i));\n');
fprintf(fileID,'  LinearConstrLo(w,h,i)             ..  PthLin(w,h,i) =g= y(w,h,i)*PthLinMin(i);\n');
fprintf(fileID,'  LinearConstrUp(w,h,i)             ..  PthLin(w,h,i) =l= y(w,h,i)*PthLinMax(i);\n');
fprintf(fileID,'  dmalu(w,h)                        ..  dmal(w,h) =e= (sum(i,y(w,h,i)*b(i))+sum(i,PthLin(w,h,i)*m(i)))-PRLProdLoss(w);\n');
fprintf(fileID,'  AluTarget(w)                      ..  sum(h,dmal(w,h)) + mPurch(w) =e= card(h)*Pth0*eta0;\n');
fprintf(fileID,'  Pth.lo(w,h) = Pth0 - PthVar;\n');
fprintf(fileID,'  Pth.up(w,h) = Pth0 + PthVar;\n');

%% Write Solve statements
fprintf(fileID,'\nModel    scheduling    / all /;\n');
fprintf(fileID,'\noption reslim = 100000\n');
fprintf(fileID,'       threads = 4\n');
fprintf(fileID,'       optcr = 0.0\n');
fprintf(fileID,'       optca = 0.0;\n');

fprintf(fileID,'  currConstr(w) =  constr("weekly",w);\n');
fprintf(fileID,'  Eth.lo(w,h)     = -EthMax("limited");\n');
fprintf(fileID,'  Eth.up(w,h)     =  EthMax("limited");\n');
fprintf(fileID,'\n  solve scheduling minimizing ctot using mip;\n');

%% Save solution and return problem status
fprintf(fileID,'returnStat(''modelstat'') = scheduling.modelstat;\n');
fprintf(fileID,'returnStat(''solvestat'') = scheduling.solvestat;\n');
fprintf(fileID,'execute_unload %%matout%%;');

fclose('all');
end
