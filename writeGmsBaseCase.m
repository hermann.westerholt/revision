function [] = writeGmsBaseCase()
%WRITEGMS Creates a .gms file with input-dependent symbol declarations. The
%   assignment of input values to parameters is done in a separate step.

%% Write .gms file
fileID = fopen('BaseCase.gms','w');
fprintf(fileID,'$set matout "''matsol.gdx''";\n');%, PthSol, EthSol, dmalSol, malSol, CSol, y, PthLin, mSold, returnStat ";\n');

%% Define Sets
fprintf(fileID,'Sets\n');
fprintf(fileID,'  w              Weeks in a year\n');
fprintf(fileID,'  h              Hours in a week\n');
fprintf(fileID,'  rec            Energy balance reconciliation intervall\n');
fprintf(fileID,'  lim            Toggle for limit on energy surplus\n');
fprintf(fileID,'  constr(rec,w)  All constraints based on reconciliation intervalls\n');
fprintf(fileID,'  currConstr(w)  Constraints used in current loop\n');
fprintf(fileID,'  i              Intervalls for linearization\n');
fprintf(fileID,'  stat           Set for model and solver status /modelstat,solvestat/;\n');
fprintf(fileID,';\n');

%% Define Scalars
fprintf(fileID,'\nScalars\n');
fprintf(fileID,'  Pth0    Nominal power consumption [MW]\n');
fprintf(fileID,'  PthVar  Maximal allowed power variation [MW]\n');
fprintf(fileID,'  eta0    Nominal production efficiency rate [t per MWh]\n');
fprintf(fileID,'  meanae  Mean absolute error of linearization\n');
fprintf(fileID,'  maxae   Max absolute error of linearization\n');
fprintf(fileID,'  maxre   Max relative error of linearization\n');
fprintf(fileID,';\n');

%% Define Parameters
fprintf(fileID,'\nParameters \n');
fprintf(fileID,'  EthMax(lim)               Maximal allowed thermal energy surplus [MWh]\n');
fprintf(fileID,'  DAP(w,h)                  Day-Ahead electricity price for each hour of the year [EUR per MW]\n');
fprintf(fileID,'  AlP(w)                    Average aluminium price for each week [EUR per t]\n');
fprintf(fileID,'  PRL(w)                    Primary power reserve in week w [MW]\n');
fprintf(fileID,'  PRLSP(w)                  PRL service price [EUR per MW]\n');
fprintf(fileID,'  PRLProdLoss(w)            Weekly production loss attributed to PRL [t]\n');
fprintf(fileID,'  PthLinMin(i)              Lower bound in intervall i\n');
fprintf(fileID,'  PthLinMax(i)              Upper bound in intervall i\n');
fprintf(fileID,'  m(i)                      Slope in intervall i\n');
fprintf(fileID,'  b(i)                      Intercept in intervall i\n');
fprintf(fileID,'  returnStat(rec,lim,stat)  Parameter for model and solver status;\n');
fprintf(fileID,';\n');

%% Load input data
fprintf(fileID,'\n$if exist matdata.gms $include matdata.gms\n');

%% Define Variables
fprintf(fileID,'\nVariables \n');
fprintf(fileID,'  Pth(w,h)             Power uptake in hour h [MW]\n');
fprintf(fileID,'  PthLin(w,h,i)        Power uptake in current intervall in hour h [MW]\n');
fprintf(fileID,'  dmal(w,h)            Marginal aluminium production in hour h [t per h]\n');
fprintf(fileID,'  Eth(w,h)             Accumulated thermal energy surplus [MWh]\n');
fprintf(fileID,'  mal(w,h)             Accumulated aluminium production [t]\n');
fprintf(fileID,'  C(w)                 Weekly production cost [EUR]\n');
fprintf(fileID,'  Ctot                 Total cost [EUR]\n');
fprintf(fileID,'  PthSol(rec,lim,w,h)  Solution vector for thermal power [MW]\n');
fprintf(fileID,'  EthSol(rec,lim,w,h)  Solution vector for thermal energy surplus [MWh]\n');
fprintf(fileID,'  dmalSol(rec,lim,w,h) Solution vector for production power [MW]\n');
fprintf(fileID,'  malSol(rec,lim,w,h)  Solution vector for production surplus [MWh]\n');
fprintf(fileID,'  CSol(rec,lim,w)      Solution vector for weekly costs [EUR]\n');
fprintf(fileID,'  CtotSol(rec,lim)     Solution vector for total costs [EUR]\n');
fprintf(fileID,';\n');

fprintf(fileID,'\nBinary variables \n');
fprintf(fileID,'  y(w,h,i)             Indicator for active intervall\n');
fprintf(fileID,';\n');

%% Define Equations
fprintf(fileID,'\nEquations \n');
fprintf(fileID,'  CostTotal              Total production cost\n');
fprintf(fileID,'  CostWeek(w)            Weekly production cost\n');
fprintf(fileID,'  Start_th               Thermal energy surplus in first hour has to equal zero\n');
fprintf(fileID,'  Start_al               Production surplus in first hour has to equal zero\n');
fprintf(fileID,'  Storage_th(w,h)        Thermal energy surplus in hour h\n');
fprintf(fileID,'  Transfer_th(w)         Transfer thermal energy surplus to next week\n');
fprintf(fileID,'  Storage_al(w,h)        Production surplus in hour h\n');
fprintf(fileID,'  Transfer_al(w)         Transfer production surplus to next week\n');
fprintf(fileID,'  Reconc(w)              Force Eth to return to zero in regular intervalls\n');
fprintf(fileID,'  LinearClosing(w,h)     Only one mode can be active\n');
fprintf(fileID,'  LinearSum(w,h)         Sum over piecewise linear parts\n');
fprintf(fileID,'  LinearConstrLo(w,h,i)  Minimum x value of current section of pwlf\n');
fprintf(fileID,'  LinearConstrUp(w,h,i)  Maximum x value of current section of pwlf\n');
fprintf(fileID,'  dmalu(w,h)             Marginal aluminium production in hour h\n');
fprintf(fileID,';\n');

fprintf(fileID,'  CostTotal                         ..  Ctot =e= sum(w,C(w));\n');
fprintf(fileID,'  CostWeek(w)                       ..  C(w) =e= sum(h, Pth(w,h) * DAP(w,h));\n');
fprintf(fileID,'  Start_th                          ..  Eth("1","1") =e= 0;\n');
fprintf(fileID,'  Start_al                          ..  mal("1","1") =e= 0;\n');
fprintf(fileID,'  Storage_th(w,h)$(ord(h)<card(h))  ..  Eth(w,h+1) =e= Eth(w,h) + Pth(w,h) - Pth0;\n');
fprintf(fileID,'  Transfer_th(w)$(ord(w)<card(w))   ..  Eth(w+1,"1") =e= Eth(w,"168") + Pth(w,"168") - Pth0;\n');
fprintf(fileID,'  Storage_al(w,h)$(ord(h)<card(h))  ..  mal(w,h+1) =e= mal(w,h) + dmal(w,h);\n');
fprintf(fileID,'  Transfer_al(w)$(ord(w)<card(w))   ..  mal(w+1,"1") =e= mal(w,"168") + dmal(w,"168");\n');
fprintf(fileID,'  Reconc(w)$currConstr(w)           ..  Pth(w,"168") - Pth0 =e= -Eth(w,"168");\n');
fprintf(fileID,'  LinearClosing(w,h)                ..  1 =e= sum(i,y(w,h,i));\n');
fprintf(fileID,'  LinearSum(w,h)                    ..  Pth(w,h) =e= sum(i,PthLin(w,h,i));\n');
fprintf(fileID,'  LinearConstrLo(w,h,i)             ..  PthLin(w,h,i) =g= y(w,h,i)*PthLinMin(i);\n');
fprintf(fileID,'  LinearConstrUp(w,h,i)             ..  PthLin(w,h,i) =l= y(w,h,i)*PthLinMax(i);\n');
fprintf(fileID,'  dmalu(w,h)                        ..  dmal(w,h) =e= (sum(i,y(w,h,i)*b(i))+sum(i,PthLin(w,h,i)*m(i)))-PRLProdLoss(w);\n');
fprintf(fileID,'  Pth.lo(w,h) = Pth0 - PthVar;\n');
fprintf(fileID,'  Pth.up(w,h) = Pth0 + PthVar;\n');

%% Write Solve statements
fprintf(fileID,'\nModel    scheduling    / all /;\n');
fprintf(fileID,'\noption reslim = 100000\n');
fprintf(fileID,'       threads = 4\n');
fprintf(fileID,'       optcr = 0.0\n');
fprintf(fileID,'       optca = 0.0;\n');

fprintf(fileID,'\nloop((rec,lim),\n');
fprintf(fileID,'  currConstr(w) =  constr(rec,w);\n');
fprintf(fileID,'  Eth.lo(w,h)     = -EthMax(lim);\n');
fprintf(fileID,'  Eth.up(w,h)     =  EthMax(lim);\n');
fprintf(fileID,'\n  solve scheduling minimizing ctot using mip;\n');
fprintf(fileID,'\n  PthSol.lo(rec,lim,w,h) = Pth.lo(w,h);\n');
fprintf(fileID,'  PthSol.l(rec,lim,w,h)  = Pth.l(w,h);\n');
fprintf(fileID,'  PthSol.up(rec,lim,w,h) = Pth.up(w,h);\n');
fprintf(fileID,'  PthSol.m(rec,lim,w,h)  = Pth.m(w,h);\n');
fprintf(fileID,'  EthSol.lo(rec,lim,w,h) = Eth.lo(w,h);\n');
fprintf(fileID,'  EthSol.l(rec,lim,w,h)  = Eth.l(w,h);\n');
fprintf(fileID,'  EthSol.up(rec,lim,w,h) = Eth.up(w,h);\n');
fprintf(fileID,'  EthSol.m(rec,lim,w,h)  = Eth.m(w,h);\n');
fprintf(fileID,'  CSol.lo(rec,lim,w)     = C.lo(w);\n');
fprintf(fileID,'  CSol.l(rec,lim,w)      = C.l(w);\n');
fprintf(fileID,'  CSol.up(rec,lim,w)     = C.up(w);\n');
fprintf(fileID,'  CSol.m(rec,lim,w)      = C.m(w);\n');
fprintf(fileID,'  CtotSol.lo(rec,lim)    = Ctot.lo;\n');
fprintf(fileID,'  CtotSol.l(rec,lim)     = Ctot.l;\n');
fprintf(fileID,'  CtotSol.up(rec,lim)    = Ctot.up;\n');
fprintf(fileID,'  CtotSol.m(rec,lim)     = Ctot.m;\n');
fprintf(fileID,'\n  dmalSol.lo(rec,lim,w,h) = dmal.lo(w,h);\n');
fprintf(fileID,'  dmalSol.l(rec,lim,w,h)  = dmal.l(w,h);\n');
fprintf(fileID,'  dmalSol.up(rec,lim,w,h) = dmal.up(w,h);\n');
fprintf(fileID,'  dmalSol.m(rec,lim,w,h)  = dmal.m(w,h);\n');
fprintf(fileID,'  malSol.lo(rec,lim,w,h) = mal.lo(w,h);\n');
fprintf(fileID,'  malSol.l(rec,lim,w,h)  = mal.l(w,h);\n');
fprintf(fileID,'  malSol.up(rec,lim,w,h) = mal.up(w,h);\n');
fprintf(fileID,'  malSol.m(rec,lim,w,h)  = mal.m(w,h);\n');
fprintf(fileID,'  returnStat(rec,lim,''modelstat'') = scheduling.modelstat;\n');
fprintf(fileID,'  returnStat(rec,lim,''solvestat'') = scheduling.solvestat;\n');
fprintf(fileID,');\n');

%% Save solution and return problem status


fprintf(fileID,'execute_unload %%matout%%;');

fclose('all');
end
