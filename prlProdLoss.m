function[PRLProdLoss] = prlProdLoss(PRL,eta0,Pth0,delPRL,delProdLoss,ProdLossMode)

    PRLIntvll = [-delPRL 0 delPRL];
    ProdLossIntvll = [eta0*Pth0*delProdLoss 0 eta0*Pth0*delProdLoss];
    if strcmp(ProdLossMode,'q')
        coeff = polyfit(PRLIntvll,ProdLossIntvll,2);
    elseif strcmp(ProdLossMode,'l')
        coeff = polyfit(PRLIntvll(2:3),ProdLossIntvll(2:3),1);
    end
     PRLProdLoss = polyval(coeff,PRL);

     X = linspace(-(delPRL+10),delPRL+10,1000);
     Y = polyval(coeff,X);
     plot(X,Y);
     hold on
     plot(PRL,PRLProdLoss,'go');
%      line([0 delPRL],[1-delProdLoss 1-delProdLoss]);
%      xlim([0 inf]);
end