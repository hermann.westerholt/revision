function [m,b,PthLinMin,PthLinMax,meanae,maxae,maxre] = pwlf(Pth0,PthVar,eta0,etaVar,etaNodes)
%     Pth0=90;PthVar=22.5;eta0=1/14;etaVar=10/100;etaNodes=1;
    % eta does not depend on PthVar: for Pth0+/-25%, eta  
    PthInt = [Pth0*(1-0.25), Pth0, Pth0*(1+0.25)];
    etaMin = eta0*(1-etaVar/(1+etaVar));
    eta = [etaMin, eta0, etaMin];
    coeff = polyfit(PthInt,eta,2);
%     x = linspace(Pth0-30,Pth0+30,100);
%     y = polyval(coeff,x);
%     plot(x,y);
%     xlim([Pth0 inf]);
%     hold on
%     plot(Pth0+PthVar,polyval(coeff,Pth0+PthVar),'go');
%     hold off
    deriv = [3*coeff(1), 2*coeff(2), coeff(3)];
    
    PthGrid = linspace(Pth0-PthVar,Pth0+PthVar,etaNodes);
    PthGrid = movmean(PthGrid,[0 1]);
    PthGrid = PthGrid(1:end-1);
    PthGrid = [PthGrid(1:end/2) Pth0 PthGrid(end/2+1:end)];
    PalGrid = polyval(coeff,PthGrid).*PthGrid;
    m = polyval(deriv,PthGrid);
    b = PalGrid - m.*PthGrid;
    
    intersec=ones(length(b)-1,1);
    for i=1:length(b)-1
        intersec(i) = (b(i)-b(i+1))/(m(i+1)-m(i));
    end
    
    PthLinMin = [PthInt(1) intersec'];
    PthLinMax = [intersec' PthInt(3)];
    
    PthNodes = [PthInt(1) intersec' PthInt(3)];
    PalNodes = ones(size(PthNodes));
    for i=1:length(PthNodes)-1
        PalNodes(i) = PthNodes(i)*m(i)+b(i);
    end
    PalNodes(end) = PthNodes(end)*m(end)+b(end);
    
    X = linspace(Pth0-PthVar,Pth0+PthVar,1000);
    Y = X.*polyval(coeff,X);
%     figure
%     plot(X,Y);
%     hold on
%     plot(PthNodes,PalNodes);
%     hold off
    % increase resolution of linear function to level of plotted function
    % (X,Y)
    PalSpline = interp1(PthNodes,PalNodes,X);
    % abs error
    ae = abs(PalSpline-Y);
    % max abs error
    [maxae, maxind] = max(ae);
    % max rel error
    maxre = maxae/Y(maxind);
    % mean abs error
    meanae = mean(ae);
end