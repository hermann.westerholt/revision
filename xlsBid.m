function [muPRL,sigPRL,muLogF1,sigmLogF1,movAvg,f2] = xlsBid(perturbMode,chosenWeek)
    file = 'Perturb.xlsx';
    muPRLrange = 'I2';
    otherRange = 'K2:N2';
    sheet = ['Week',int2str(chosenWeek),perturbMode];
    muPRL = xlsread(file,sheet,muPRLrange);
    sigPRL = 0.1*muPRL;
    other = xlsread(file,sheet,otherRange);
    muLogF1 = other(1);
    sigmLogF1 = other(2);
    movAvg = other(3);  
    f2 = other(4);
    end
end