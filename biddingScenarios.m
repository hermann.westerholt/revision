function[scenIdx, setMax, mapVal, PRLval, CPRLval] = biddingScenarios(PRLvec, ...
    PRLcell, CPRL, idxBidding)

    %% Create sets and Parameters for all scenarios
    scenIdx = size(PRLcell,1);
    for scenIdx = 1:scenIdx
        iRows = [scenIdx*ones(numel(PRLcell{scenIdx, 2}), 1) [1:numel(PRLcell{scenIdx, 2})]'];
        PRLRows = [iRows, PRLcell{scenIdx, 2}'];
        CPRLRows = [iRows, PRLcell{scenIdx, 3}'];
        if scenIdx ==1
            mapVal = iRows;
            PRLval = PRLRows;
            CPRLval = CPRLRows;
        else
            mapVal = vertcat(mapVal,iRows);
            PRLval = vertcat(PRLval,PRLRows);
            CPRLval = vertcat(CPRLval,CPRLRows);
        end
    end
    setMax = max(mapVal(:,2));
    i = idxBidding;
    for w=1:size(CPRLval,1)
        j = find(PRLvec == CPRLval(w,3));
        CPRLval(w,3) = CPRL(i,j);
    end
end