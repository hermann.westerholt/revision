function [] = writeGmsBid(bidMode)
%WRITEBIDGMS Creates a .gms file with input-dependent symbol declarations. The
%   assignment of input values to parameters is done in a separate step.

%% Write .gms file
fileID = fopen('Bidding.gms','w');
fprintf(fileID,'$set matout "''matsol.gdx''";\n');
fprintf(fileID,'display "%%system.filesys%%/";\n');
fprintf(fileID,'file screen /''con''/;\n');
fprintf(fileID,'\n$macro erf(x) (1/2 + 1/sqrt(2*pi)*exp(-power(x,2)/2)*(x + power(x,3)/3 + power(x,5)/15 + power(x,7)/105 + power(x,9)/945 + power(x,11)/10395 + power(x,13)/135135 + power(x,15)/2027025 + power(x,17)/34459425 + power(x,19)/654729075 + power(x,21)/13749310575 + power(x,23)/316234143225))\n');% + power(x,25)/7905853580625 + power(x,27)/213458046676875))\n');% + power(x,29)/(213458046676875*29)))\n');

%% Define sets
fprintf(fileID,'\nSets\n');
fprintf(fileID,'  scenIdx         Loop index for scenarios\n');
fprintf(fileID,'  ii              Global set elements for all partitions\n');
fprintf(fileID,'  map(scenIdx,ii) Scenario-to-set mapping\n');
fprintf(fileID,'  i(ii)           Set elements in current loop\n');
fprintf(fileID,'  stat            Set for model and solver status /modelstat,solvestat/\n');
fprintf(fileID,';\n');

%% Define aliases
fprintf(fileID,'\nalias(ii,j);\n');

%% Define Scalars
fprintf(fileID,'\nScalars\n');
if strcmp(bidMode,'knownPrices')
    fprintf(fileID,'  mu                   Median price of accepted offer [EUR]\n');
    fprintf(fileID,'  sigm                 Standard price deviation of accepted offers [EUR]\n');
elseif strcmp(bidMode,'estimPrices')
    fprintf(fileID,'  mu                   Median of factor f1''s natural logarithm [-]\n');
    fprintf(fileID,'  sigm                 Standard deviation of factor f1''s natural logarithm [-]\n');
    fprintf(fileID,'  movAvg               Moving average of past ask prices [EUR]\n');
    fprintf(fileID,'  f2                   Price estimation factor f2\n');
end
fprintf(fileID,';\n');

%% Define Parameters
fprintf(fileID,'\nParameters \n');
fprintf(fileID,'  returnStat(scenIdx,stat) Parameter for model and solver status\n');
fprintf(fileID,'  tSol(scenIdx)            Time used by solver [CPU secs]\n');
fprintf(fileID,'  C0                       Production cost without PRL [EUR]\n');
fprintf(fileID,'  allCPRL(scenIdx,ii)      Production cost with PRL for all scenarios and weeks [EUR]\n');
fprintf(fileID,'  CPRL(ii)                 Production cost for given scenario [EUR]\n');
fprintf(fileID,'  allPRL(scenIdx,ii)       PRL partitions for all scenarios [MW]\n');
fprintf(fileID,'  PRL(ii)                  PRL partition for given scenario [MW]\n');
fprintf(fileID,';\n');

%% Load input data
fprintf(fileID,'\n$if exist matdata.gms $include matdata.gms\n');

%% Define Variables
fprintf(fileID,'\nVariables \n');
fprintf(fileID,'  askPrice(ii)            Price for current PRL offer\n');
fprintf(fileID,'  x(ii)                   Transformed ask price\n');
if strcmp(bidMode,'estimPrices')
    fprintf(fileID,'  y1(ii)                  Natural logarithm of price estimation factor f1\n');
end
fprintf(fileID,'  pRefuse                 Probabilty that entire offer package is refused\n');
fprintf(fileID,'  p(ii)                   Probabilty that bid package i is accepted\n');
fprintf(fileID,'  C                       Weekly cost in current loop [EUR]\n');
fprintf(fileID,'  askPriceSol(scenIdx,ii) Solution vector for ask price [EUR]\n');
fprintf(fileID,'  xSol(scenIdx,ii)        Solution vector for transformed ask price [-]\n');
fprintf(fileID,'  pSol(scenIdx,ii)        Solution vector for probabilities [-]\n');
fprintf(fileID,'  CSol(scenIdx)           Solution vector for weekly cost [EUR]\n');
fprintf(fileID,';\n');

fprintf(fileID,'\nPositive variables \n');
fprintf(fileID,'  pRefuse,p,askPrice\n');
fprintf(fileID,';\n');

%% Define Equations
fprintf(fileID,'\nEquations \n');
fprintf(fileID,'  CostWeek         Weekly production cost\n');
fprintf(fileID,'  Transf(ii)       Transform offer price into standard normally distributed variable\n');
fprintf(fileID,'  ProbRefuse       Probability of entire offer refusal\n');
fprintf(fileID,'  ProbAcc(ii)      Probability of partial offer acceptance\n');
fprintf(fileID,'  ProbAccLast(ii)  Probability of entire offer acceptance\n');
fprintf(fileID,'  Order(ii)        Order results\n');
fprintf(fileID,'  pZero(ii)        Avoid unnecessary nonzero values\n');
if strcmp(bidMode,'estimPrices')
    fprintf(fileID,'  Estimation(ii)   Linking factors to asked price\n');
end
fprintf(fileID,';\n');

fprintf(fileID,'  CostWeek                          ..  C =e= pRefuse*C0 + sum(ii$(i(ii)),p(ii)*(CPRL(ii) - sum(j$(ord(j) le ord(ii)),(PRL(j)*askPrice(j)))));\n');
if strcmp(bidMode,'knownPrices')
    fprintf(fileID,'  Transf(ii)$i(ii)                  ..  x(ii) =e= (askPrice(ii) - mu)/(sigm);\n');
elseif strcmp(bidMode,'estimPrices')
    fprintf(fileID,'  Transf(ii)$i(ii)                  ..  x(ii) =e= (y1(ii) - mu)/(sigm);\n');
end
fprintf(fileID,'  ProbRefuse                        ..  pRefuse =e= 1 - sum(ii$(i(ii)),p(ii));\n');
fprintf(fileID,'  ProbAcc(ii)$(ord(ii)<card(i))     ..  p(ii) =e= erf(x(ii+1))-erf(x(ii));\n');
fprintf(fileID,'  ProbAccLast(ii)$(ord(ii)=card(i)) ..  p(ii) =e= 1-erf(x(ii));\n');
fprintf(fileID,'  Order(ii)$(ord(ii)<card(i))       ..  askPrice(ii) =l= askPrice(ii+1);\n');
fprintf(fileID,'  pZero(ii)$(ord(ii)>card(i))       ..  p(ii) =e= 0;\n');
if strcmp(bidMode,'estimPrices')
    fprintf(fileID,'  Estimation(ii)$i(ii)              ..  y1(ii) =e= log(askPrice(ii)) - log(movAvg) - log(f2);\n');
end

fprintf(fileID,'\n  x.lo(ii)             = -2;\n');
fprintf(fileID,'  x.up(ii)             = 2;\n');
if strcmp(bidMode,'knownPrices')
    fprintf(fileID,'  askPrice.lo(ii)      = mu - 2*sigm;\n');
    fprintf(fileID,'  askPrice.up(ii)      = mu + 2*sigm;\n');
elseif strcmp(bidMode,'estimPrices')
    fprintf(fileID,'  askPrice.lo(ii)      = 0;\n');
    fprintf(fileID,'  askPrice.up(ii)      = movAvg*3;\n');
    fprintf(fileID,'  y1.lo(ii)            = mu - 2*sigm;\n');
    fprintf(fileID,'  y1.up(ii)            = mu + 2*sigm;\n');
end
fprintf(fileID,'  p.lo(ii)             = 0;\n');
fprintf(fileID,'  p.up(ii)             = 1;\n');
fprintf(fileID,'  pRefuse.lo           = 0;\n');
fprintf(fileID,'  pRefuse.up           = 1;\n');

%% Write Solve statements
fprintf(fileID,'\nModel    bidding    / all /;\n');
fprintf(fileID,'\noption NLP = Baron\n');
fprintf(fileID,'       reslim = 100000\n');
fprintf(fileID,'       threads = 6\n');
fprintf(fileID,'       profile = 3\n');
fprintf(fileID,'       profiletol = 30\n');
fprintf(fileID,'       optca = 1\n');
fprintf(fileID,'       optcr = 1e-5;\n');

fprintf(fileID,'$onecho > baron.opt\n');
fprintf(fileID,'DeltaTerm 1\n');
fprintf(fileID,'DeltaT 600\n');
fprintf(fileID,'DeltaA 0.5\n');
fprintf(fileID,'$offecho\n');

fprintf(fileID,'\nloop(scenIdx,\n');
fprintf(fileID,'  i(ii)                = map(scenIdx,ii);\n');
fprintf(fileID,'  CPRL(ii)             = allCPRL(scenIdx,ii);\n');
fprintf(fileID,'  PRL(ii)              = allPRL(scenIdx,ii);\n');
fprintf(fileID,'  p.l(ii)$i(ii)        = 0.3;\n');
if strcmp(bidMode,'knownPrices')
    fprintf(fileID,'  askPrice.l(ii)$i(ii) = mu;\n');
elseif strcmp(bidMode,'estimPrices')
    fprintf(fileID,'  askPrice.l(ii)$i(ii) = movAvg;\n');
    fprintf(fileID,'  y1.l(ii)$i(ii) = - log(f2);\n');
end
% fprintf(fileID,'    bidding.optfile = 1\n');
fprintf(fileID,'\n  solve bidding minimizing c using nlp;\n');

fprintf(fileID,'\n  CSol.lo(scenIdx)           = C.lo;\n');
fprintf(fileID,'  CSol.l(scenIdx)            = C.l;\n');
fprintf(fileID,'  CSol.up(scenIdx)           = C.up;\n');
fprintf(fileID,'  CSol.m(scenIdx)            = C.m;\n');
fprintf(fileID,'  askPriceSol.lo(scenIdx,ii) = askPrice.lo(ii);\n');
fprintf(fileID,'  askPriceSol.l(scenIdx,ii)  = askPrice.l(ii);\n');
fprintf(fileID,'  askPriceSol.up(scenIdx,ii) = askPrice.up(ii);\n');
fprintf(fileID,'  askPriceSol.m(scenIdx,ii)  = askPrice.m(ii);\n');
fprintf(fileID,'  xSol.lo(scenIdx,ii)        = x.lo(ii);\n');
fprintf(fileID,'  xSol.l(scenIdx,ii)         = x.l(ii);\n');
fprintf(fileID,'  xSol.up(scenIdx,ii)        = x.up(ii);\n');
fprintf(fileID,'  xSol.m(scenIdx,ii)         = x.m(ii);\n');
fprintf(fileID,'  pSol.lo(scenIdx,ii)        = p.lo(ii);\n');
fprintf(fileID,'  pSol.l(scenIdx,ii)         = p.l(ii);\n');
fprintf(fileID,'  pSol.up(scenIdx,ii)        = p.up(ii);\n');
fprintf(fileID,'  pSol.m(scenIdx,ii)         = p.m(ii);\n');
fprintf(fileID,'  tSol(scenIdx)              = bidding.resUsd;\n');
fprintf(fileID,'  returnStat(scenIdx,''modelstat'') = bidding.modelstat;\n');
fprintf(fileID,'  returnStat(scenIdx,''solvestat'') = bidding.solvestat;\n');
fprintf(fileID,'  putclose screen ''The current value of scenIdx is '',scenIdx.tl/\n');
fprintf(fileID,');\n');

%% Save solution and return problem status;
fprintf(fileID,'execute_unload %%matout%%;');

fclose('all');
end
